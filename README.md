Sealed Secret Controller
========================

This role enables the installation of the sealed-secrets-controller via ansible role sealed-secrets.
The ansible role uses an helm chart to install the sealed-secrets-controller.
The used helm values file can be found  in templates/helmvalues.yml

As stated in the documentation https://github.com/helm/charts/tree/master/stable/sealed-secrets 
the SecurityContext is reconfigured when the sealed-secrets-controller is installed on openshift cluster.


Install client-side tool into /usr/local/bin/
==================================================

This clientside tool is used to create sealed secrets and can be locally installed.

```
Download: wget https://github.com/bitnami-labs/sealed-secrets/releases/download/v0.12.4/kubeseal-amd64
sudo install -m 755 kubeseal-amd64 /usr/local/bin/kubeseal
```

Create a sealed Secret file.
============================


**note the use of `--dry-run` - this does not create a secret in your cluster**
```
kubectl create secret generic secret-name --dry-run --from-literal=foo=bar -o [json|yaml] | \
 kubeseal \
 --controller-name=sealed-secrets \
 --controller-namespace=sp-sealed-secrets \
 --format [json|yaml] > mysealedsecret.[json|yaml]
```


The file mysealedsecret.[json|yaml] is a committable file.

If you would rather not need access to the cluster to generate the sealed secret you can run

```
kubeseal \
 --controller-name=sealed-secrets \
 --controller-namespace=sp-sealed-secrets \
 --fetch-cert > mycert.pem
```


to retrieve the public cert used for encryption and store it locally. You can then run 'kubeseal --cert mycert.pem' instead to use the local cert e.g.

```
kubectl create secret generic secret-name --dry-run --from-literal=foo=bar -o [json|yaml] | \
kubeseal \
 --controller-name=sealed-secrets \
 --controller-namespace=sp-sealed-secrets \
 --format [json|yaml] --cert mycert.pem > mysealedsecret.[json|yaml]
```


Apply the sealed secret
==========================

```
kubectl create -f mysealedsecret.[json|yaml]
Running 'kubectl get secret secret-name -o [json|yaml]' will show the decrypted secret that was generated from the sealed secret.
```


Both the SealedSecret and generated Secret must have the same name and namespace.


Run test-suite in platform-toolbox
==========================
/playbook/deploy.sh -s -f ./config/config-dev.yaml sealed-secrets -t test -v
